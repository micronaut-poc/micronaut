import http from 'k6/http';
import { create } from './create-script.js';
import { query } from './query-script.js';

let testToRun = [
  create,
  query,
];

export const options = {
  discardResponseBodies: true,
  scenarios: {
    create: {
      executor: 'constant-vus',
      exec: 'exec_create',
      vus: 10,
      //iterations: 100,
      duration: '60s',
    },
    query: {
          executor: 'constant-vus',
          exec: 'exec_query',
          vus: 10,
          //iterations: 100,
          duration: '60s',
    },
  },
};

export function exec_create() {
  create.fn();
}
export function exec_query() {
  query.fn();
}
//export default function () {
//  for (const test of testToRun) {
//    test.fn();
//  }
//}

