import { check } from "k6";
import http from "k6/http";

export const query = {
  name: 'query-test',
  fn: test
}

function test() {
  let res = http.get("http://localhost:8080/1");
  check(res, {
    "is status 200": (r) => r.status === 200
  });
};