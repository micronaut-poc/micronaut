import { check } from "k6";
import http from "k6/http";

export const create = {
  name: 'create-test',
  fn: test,
}

function test() {
	const headers = {
		headers: {
            'Content-Type': 'application/json'
	      }
	};

  const createRequest = JSON.stringify({ name: "micronaut-demo", bornDate: "2000-10-03"});
  let res = http.post("http://localhost:8080/users", createRequest, headers);
  check(res, {
    "is status 200": (r) => r.status === 200
  });
};