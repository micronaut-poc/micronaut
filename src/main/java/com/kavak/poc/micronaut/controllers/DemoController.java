package com.kavak.poc.micronaut.controllers;

import com.kavak.poc.micronaut.user.adapters.outbound.rest.dto.UserDto;
import com.kavak.poc.micronaut.user.adapters.outbound.rest.repository.UserAPIRepository;
import com.kavak.poc.micronaut.user.domain.model.User;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import reactor.core.publisher.Mono;

@Controller(produces = "application/json", consumes = "application/json")
@ExecuteOn(TaskExecutors.IO)
public class DemoController {

  @Inject private UserAPIRepository userAPIRepository;

  @Get("/{id}")
  public Mono<UserDto> index(@PathVariable("id") String id) {
    return userAPIRepository.get(id);
  }

  @Post
  public Mono<UserDto> create(@Body User body) {
    return userAPIRepository.create(body);
  }
}
