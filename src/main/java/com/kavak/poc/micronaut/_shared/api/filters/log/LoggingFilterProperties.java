package com.kavak.poc.micronaut._shared.api.filters.log;

import io.micronaut.context.annotation.ConfigurationProperties;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(LoggingFilterProperties.PREFIX)
public class LoggingFilterProperties {
  public static final String PREFIX = "logging.filter";

  private Set<String> excludeUrls;
  private Set<String> excludeHeaders;
}
