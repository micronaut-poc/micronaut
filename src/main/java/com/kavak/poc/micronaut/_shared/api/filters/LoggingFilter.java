package com.kavak.poc.micronaut._shared.api.filters;

import com.kavak.poc.micronaut._shared.api.HttpStatusHelper;
import com.kavak.poc.micronaut._shared.api.filters.log.HttpLog;
import com.kavak.poc.micronaut._shared.api.filters.log.LoggingFilterProperties;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.json.JsonMapper;
import jakarta.inject.Singleton;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Singleton
public class LoggingFilter {

  private final JsonMapper mapper;

  private LoggingFilterProperties properties;

  protected boolean shouldNotFilter(HttpRequest request) {
    return properties.getExcludeUrls().stream()
        .anyMatch(exclude -> exclude.equalsIgnoreCase(request.getPath()));
  }

  public void logRequest(HttpRequest request, MutableHttpResponse response, long startTime) {
    // si aplica el filtro en la url!

    try {
      var logMessage = buildLogMessage(request, response, startTime);
      var logMsgAsString = mapper.writeValueAsString(logMessage);
      Optional.of(response.getStatus())
          .filter(httpStatus -> HttpStatusHelper.isError(httpStatus))
          .ifPresentOrElse(
              httpStatus -> logError(httpStatus, logMsgAsString), () -> log.info(logMsgAsString));
    } catch (IOException e) {
      log.error("???", e);
    }
  }

  private void logError(HttpStatus httpStatus, String logMessage) {
    if (HttpStatusHelper.is4xxClientError(httpStatus)) {
      log.warn(logMessage);
    } else {
      log.error(logMessage);
    }
  }

  private HttpLog buildLogMessage(
      HttpRequest request, MutableHttpResponse response, long startTime) {
    var requestHeaders = getHeadersAsMap(request.getHeaders(), properties.getExcludeHeaders());
    var responseHeaders = getHeadersAsMap(response.getHeaders(), properties.getExcludeHeaders());

    var logMessageBuilder =
        HttpLog.builder()
            .remoteHost(request.getRemoteAddress().getHostName())
            .method(request.getMethodName())
            .url(request.getUri().toString())
            .requestHeaders(requestHeaders)
            .status(response.getStatus().getCode())
            .responseHeaders(responseHeaders)
            .elapsedTime(System.currentTimeMillis() - startTime);

    getPayload(request.getBody()).ifPresent(logMessageBuilder::requestPayload);
    if (log.isDebugEnabled() || HttpStatusHelper.isError(response.getStatus())) {
      getPayload(response.getBody()).ifPresent(logMessageBuilder::responsePayload);
    }

    return logMessageBuilder.build();
  }

  private Map<String, List<String>> getHeadersAsMap(
      HttpHeaders headers, Set<String> excludeHeaders) {
    Map<String, List<String>> headersMap = headers.asMap();
    excludeHeaders.forEach(exclude -> headersMap.remove(exclude));
    return headersMap;
  }

  @SneakyThrows(IOException.class)
  private Optional<String> getPayload(Optional<?> payload) {
    if (payload.isPresent()) {
      return Optional.of(mapper.writeValueAsString(payload.get()));
    }
    return Optional.empty();
  }
}
