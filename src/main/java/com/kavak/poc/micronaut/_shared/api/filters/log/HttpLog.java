package com.kavak.poc.micronaut._shared.api.filters.log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;
import io.micronaut.serde.annotation.Serdeable;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
@Serdeable
public class HttpLog implements Serializable {

  private String remoteHost;
  private String method;
  private String url;
  private Map<String, List<String>> requestHeaders;
  @JsonRawValue private String requestPayload;
  private int status;
  private Map<String, List<String>> responseHeaders;

  @JsonInclude(Include.NON_NULL)
  @JsonRawValue
  private String responsePayload;

  private long elapsedTime;
}
