package com.kavak.poc.micronaut._shared.api;

import io.micronaut.http.HttpStatus;

public abstract class HttpStatusHelper {

  public static boolean is4xxClientError(HttpStatus httpStatus) {
    return httpStatus.getCode() / 100 == 4;
  }

  public static boolean isError(HttpStatus httpStatus) {
    return httpStatus.getCode() / 100 > 3;
  }
}
