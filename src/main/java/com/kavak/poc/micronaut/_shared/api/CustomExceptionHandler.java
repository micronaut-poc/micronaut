package com.kavak.poc.micronaut._shared.api;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import io.micronaut.problem.HttpStatusType;
import jakarta.inject.Singleton;
import java.net.URI;
import java.util.NoSuchElementException;
import org.zalando.problem.Problem;

@Produces
@Singleton
@Requires(classes = {ExceptionHandler.class})
public class CustomExceptionHandler
    implements ExceptionHandler<NoSuchElementException, HttpResponse> {

  @Override
  public HttpResponse handle(HttpRequest request, NoSuchElementException exception) {
    Problem p =
        Problem.builder()
            .withType(URI.create("https://example.org/notfound"))
            .withTitle("Not found")
            .withStatus(new HttpStatusType(HttpStatus.NOT_FOUND))
            .with("id", "...")
            .build();

    return HttpResponse.notFound(p);
  }
}
