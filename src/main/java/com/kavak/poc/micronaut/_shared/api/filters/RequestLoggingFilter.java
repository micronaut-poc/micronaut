package com.kavak.poc.micronaut._shared.api.filters;

import static io.micronaut.http.annotation.Filter.MATCH_ALL_PATTERN;
import static java.lang.System.currentTimeMillis;

import io.micronaut.core.async.publisher.Publishers;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.HttpServerFilter;
import io.micronaut.http.filter.ServerFilterChain;
import io.micronaut.http.filter.ServerFilterPhase;
import jakarta.inject.Inject;
import org.reactivestreams.Publisher;

@Filter(MATCH_ALL_PATTERN)
public class RequestLoggingFilter implements HttpServerFilter {

  @Inject private LoggingFilter loggingFilter;

  @Override
  public int getOrder() {
    return ServerFilterPhase.FIRST.order();
  }

  @Override
  public Publisher<MutableHttpResponse<?>> doFilter(
      HttpRequest<?> request, ServerFilterChain chain) {

    long startTime = currentTimeMillis();

    return Publishers.then(
        chain.proceed(request), resp -> loggingFilter.logRequest(request, resp, startTime));
  }
}
