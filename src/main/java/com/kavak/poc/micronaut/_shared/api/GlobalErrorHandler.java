package com.kavak.poc.micronaut._shared.api;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.hateoas.JsonError;
import jakarta.inject.Singleton;

@Controller(value = "/noaplica")
@Singleton
public class GlobalErrorHandler {

  @Error(global = true)
  public HttpResponse<JsonError> httpClientError(
      HttpRequest request, HttpClientResponseException ex) {
    JsonError error = new JsonError("Client error");
    return HttpResponse.<JsonError>notFound().body(error);
  }

  @Error(global = true)
  public HttpResponse<JsonError> anyException(HttpRequest request, Throwable ex) {
    ex.printStackTrace();
    JsonError error = new JsonError("Internal Error");
    return HttpResponse.<JsonError>serverError().body(error);
  }
}
