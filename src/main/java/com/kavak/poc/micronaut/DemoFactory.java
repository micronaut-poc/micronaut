package com.kavak.poc.micronaut;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;
import io.micronaut.web.router.RouteBuilder.UriNamingStrategy;
import io.micronaut.web.router.naming.ConfigurableUriNamingStrategy;
import jakarta.inject.Singleton;
import java.util.Arrays;

@Factory
public class DemoFactory {

  @Singleton
  @Replaces(factory = ConfigurableUriNamingStrategy.class)
  UriNamingStrategy uriNamingStrategy() {
    return new UriNamingStrategy() {};
  }

  //  @Singleton
  //  public MeterBinder processMemoryMetrics() {
  //    return new ProcessMemoryMetrics();
  //  }

  @Singleton
  MeterFilter addCommonTagFilter() {
    return MeterFilter.commonTags(Arrays.asList(Tag.of("application", "micronaut")));
  }
}
