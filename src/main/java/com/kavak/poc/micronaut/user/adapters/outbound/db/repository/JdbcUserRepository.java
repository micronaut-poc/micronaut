package com.kavak.poc.micronaut.user.adapters.outbound.db.repository;

import com.kavak.poc.micronaut.user.adapters.outbound.db.entity.UserEntity;
import io.micronaut.data.repository.PageableRepository;
import java.util.UUID;

// @JdbcRepository(dialect = POSTGRES)
public interface JdbcUserRepository extends PageableRepository<UserEntity, UUID> {}
