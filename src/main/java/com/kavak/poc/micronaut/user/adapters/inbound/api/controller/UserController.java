package com.kavak.poc.micronaut.user.adapters.inbound.api.controller;

import com.kavak.poc.micronaut.user.adapters.inbound.api.communitation.CreateUserRequest;
import com.kavak.poc.micronaut.user.domain.actions.create.UserCreateUseCase;
import com.kavak.poc.micronaut.user.domain.actions.find.UserFindUseCase;
import com.kavak.poc.micronaut.user.domain.model.User;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import java.util.UUID;
import lombok.AllArgsConstructor;

@Controller("/users")
@ExecuteOn(TaskExecutors.IO)
@AllArgsConstructor
public class UserController {

  private final UserFindUseCase userFindUseCase;
  private final UserCreateUseCase userCreateUseCase;

  @Get(uri = "/{id}", produces = MediaType.APPLICATION_JSON)
  public User get(@PathVariable UUID id) {
    return userFindUseCase.findById(id);
  }

  @Post(produces = MediaType.APPLICATION_JSON)
  public User create(@Body CreateUserRequest body) {
    return userCreateUseCase.create(
        User.builder()
            .id(UUID.randomUUID())
            .bornDate(body.getBornDate())
            .name(body.getName())
            .build());
  }
}
