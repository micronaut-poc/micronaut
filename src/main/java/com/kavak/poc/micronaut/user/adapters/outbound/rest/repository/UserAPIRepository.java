package com.kavak.poc.micronaut.user.adapters.outbound.rest.repository;

import static io.micronaut.http.MediaType.APPLICATION_JSON;

import com.kavak.poc.micronaut.user.adapters.outbound.rest.dto.UserDto;
import com.kavak.poc.micronaut.user.domain.model.User;
import com.kavak.poc.micronaut.user.domain.ports.UserAPIPort;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.retry.annotation.CircuitBreaker;
import jakarta.inject.Singleton;
import java.util.Map;
import reactor.core.publisher.Mono;

@Singleton
public class UserAPIRepository implements UserAPIPort {

  private HttpClient httpClient;

  public UserAPIRepository(@Client(id = "user-api") HttpClient httpClient) {
    this.httpClient = httpClient;
  }

  public UserDto getBlocking(String userId) {
    MutableHttpRequest<?> req =
        HttpRequest.GET(UriBuilder.of("/{id}").expand(Map.of("id", userId)))
            .contentType(APPLICATION_JSON)
            .accept(APPLICATION_JSON);

    return httpClient.toBlocking().retrieve(req, Argument.of(UserDto.class));
  }

  @CircuitBreaker(reset = "20s")
  public Mono<UserDto> get(String userId) {

    MutableHttpRequest<?> req =
        HttpRequest.GET(UriBuilder.of("/{id}").expand(Map.of("id", userId)))
            .contentType(APPLICATION_JSON)
            .accept(APPLICATION_JSON);

    return Mono.from(httpClient.retrieve(req, Argument.of(UserDto.class)));
  }

  public Mono<UserDto> create(User user) {

    MutableHttpRequest<User> req =
        HttpRequest.POST("/", user).contentType(APPLICATION_JSON).accept(APPLICATION_JSON);

    return Mono.from(httpClient.retrieve(req, Argument.of(UserDto.class)));
  }
}
