package com.kavak.poc.micronaut.user.adapters.outbound.db.repository;

import com.kavak.poc.micronaut.user.adapters.outbound.db.entity.UserEntity;
import com.kavak.poc.micronaut.user.domain.model.User;
import com.kavak.poc.micronaut.user.domain.ports.UserRepositoryPort;
import io.micronaut.data.annotation.Repository;
import jakarta.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;

@Repository
@AllArgsConstructor
public class UserRepository implements UserRepositoryPort {
  private JpaUserRepository repository;
  //  private JdbcUserRepository repository;

  @Override
  public Optional<User> findById(UUID id) {
    Optional<UserEntity> user = repository.findById(id);
    return user.map(
        userEntity ->
            User.builder()
                .id(userEntity.getId())
                .bornDate(userEntity.getBornDate())
                .name(userEntity.getName())
                .build());
  }

  @Override
  @Transactional
  public User save(User user) {
    UserEntity userEntity =
        UserEntity.builder()
            .id(user.getId())
            .bornDate(user.getBornDate())
            .name(user.getName())
            .build();
    repository.save(userEntity);
    return user;
  }
}
