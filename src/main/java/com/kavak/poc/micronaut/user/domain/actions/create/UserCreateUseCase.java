package com.kavak.poc.micronaut.user.domain.actions.create;

import com.kavak.poc.micronaut.user.domain.model.User;

public interface UserCreateUseCase {

  User create(User userDto);
}
