package com.kavak.poc.micronaut.user.domain.model;

import io.micronaut.serde.annotation.Serdeable;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@ToString
@Serdeable
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

  private UUID id;
  private String name;
  private LocalDate bornDate;
}
