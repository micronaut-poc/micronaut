package com.kavak.poc.micronaut.user.domain.actions.find;

import com.kavak.poc.micronaut.user.domain.model.User;
import java.util.UUID;

public interface UserFindUseCase {
  User findById(UUID id);
}
