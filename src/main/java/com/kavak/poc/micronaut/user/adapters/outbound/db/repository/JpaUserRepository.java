package com.kavak.poc.micronaut.user.adapters.outbound.db.repository;

import com.kavak.poc.micronaut.user.adapters.outbound.db.entity.UserEntity;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;
import java.util.UUID;

@Repository
public interface JpaUserRepository extends JpaRepository<UserEntity, UUID> {}
