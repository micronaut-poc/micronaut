package com.kavak.poc.micronaut.user.adapters.outbound.rest.dto;

import io.micronaut.serde.annotation.Serdeable;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@Serdeable
public class UserDto {

  private String name;
  private String lastname;
  private String status;
}
