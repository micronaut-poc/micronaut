package com.kavak.poc.micronaut.user.domain.actions.create;

import com.kavak.poc.micronaut.user.domain.model.User;
import com.kavak.poc.micronaut.user.domain.ports.UserRepositoryPort;
import jakarta.inject.Singleton;
import lombok.AllArgsConstructor;

@Singleton
@AllArgsConstructor
public class UserCreateUseCaseImpl implements UserCreateUseCase {

  private UserRepositoryPort repository;

  @Override
  public User create(User user) {
    return repository.save(user);
  }
}
