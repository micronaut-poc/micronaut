package com.kavak.poc.micronaut.user.adapters.inbound.api.communitation;

import io.micronaut.serde.annotation.Serdeable;
import jakarta.validation.constraints.NotBlank;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Serdeable
public class CreateUserRequest {

  @NotBlank private String name;
  private LocalDate bornDate;
}
