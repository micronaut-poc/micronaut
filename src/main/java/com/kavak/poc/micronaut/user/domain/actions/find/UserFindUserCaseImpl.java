package com.kavak.poc.micronaut.user.domain.actions.find;

import com.kavak.poc.micronaut.user.domain.model.User;
import com.kavak.poc.micronaut.user.domain.ports.UserRepositoryPort;
import jakarta.inject.Singleton;
import java.util.UUID;
import lombok.AllArgsConstructor;

@Singleton
@AllArgsConstructor
public class UserFindUserCaseImpl implements UserFindUseCase {

  private UserRepositoryPort repository;

  @Override
  //  @Cacheable(cacheNames = "my-cache")
  public User findById(UUID id) {
    return repository.findById(id).orElseThrow();
  }
}
